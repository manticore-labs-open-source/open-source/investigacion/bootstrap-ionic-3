# **Implementar Bootstrap en un proyecto de ionic 3**

Para utilizar boopstrat en un proyecto con ionic 3, debemos crear un archivo y configurar varios que han sido creados al momento de crear un pryecto de ionic 3.

***Seguir los siguientes pasos:***

-   Instalar bootstrap con el siguiente comando.

```
$ npm install bootstrap
```

![npm](./imagenes-bootstrap/npm-bootstrap.png)

-   Crear un archivo con el nombre sass.config.js a la altura del packjson.json

![archivos](./imagenes-bootstrap/archivos.png)

-   Añadir en el archivo sass.config.js creado, las siguientes líneas:

```
module.exports = {
    includePaths: [
        'node_modules/ionic-angular/themes',
        'node_modules/ionicons/dist/scss',
        'node_modules/ionic-angular/fonts',        
        'node_modules/bootstrap/scss'
    ]
}
```

![sass-archivo](./imagenes-bootstrap/archivo-sass.png)

-   Se importara en el archivo variables.scss, el boopstrat.

```
@import "bootstrap";
```

![variables](./imagenes-bootstrap/variables.png)

-   En el archivo packjson.json para terminar añadiremos lo siguiente.

```
"config": {
    "ionic_sass": "./sass.config.js"
}
```

![packjson](./imagenes-bootstrap/packjson.png)



<a href="https://www.facebook.com/jonathan.parra.56" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en Twitter"/> Jonathan Parra </a><br>
<a href="https://www.linkedin.com/in/jonathan-parra-a89a68162/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Jonathan Parra</a><br>
<a href="https://www.instagram.com/Choco_20jp/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> Choco_20jp</a><br>